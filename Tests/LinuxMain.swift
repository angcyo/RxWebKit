import XCTest

import RxWebKitTests

var tests = [XCTestCaseEntry]()
tests += RxWebKitTests.__allTests()

XCTMain(tests)
